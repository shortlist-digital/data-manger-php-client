<?php
$appKey = 'XXXXXXXXXX';
$appSecret = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';

$postData = new stdClass();  
$postData->emailAddress = 'elliot@example.com';

if (postToDataManger($appKey, $appSecret, $postData)) {
  echo "Post successful";
} else {
  echo "Post error";
}

function postToDataManger($appKey, $appSecret, stdClass $postData) {
  $postData->appKey = $appKey;
  $postData->appSecret = $appSecret;
  $postData->nonce = hash('sha512', makeRandomString());
  $postData->signature = hash_hmac('sha1', $postData->nonce, $appSecret);

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, 'http://data-manger.io.shortlistmedia.co.uk/data-record');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData)); 
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain')); 
  $result = curl_exec($ch);
  if (curl_getinfo($ch, CURLINFO_HTTP_CODE) === 201) {
    return true;
  } 
  // echo $result;
  return false;
}

function makeRandomString($bits = 256) {
    $bytes = ceil($bits / 8);
    $return = '';
    for ($i = 0; $i < $bytes; $i++) {
        $return .= chr(mt_rand(0, 255));
    }
    return $return;
}
